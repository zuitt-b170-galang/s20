// DIFFERENCE
/*
1. quotation marks
-JS Objects only has the value inserted inside quotation marks.
-JSON has the quotation marks for both key and the value.

2. 
-JS objects - exclusive to javascript. other programming languages cannot use js object files
-JSON - not exclusive for javascript. other programming languages can also use JSON files
*/

/*
-create and log a JS object with the following properties
	city
	province
	country
*/
/*let city = {
	city: "Ormoc",
	province: "Leyte",
	country: "Philippines"
}
console.log(city);

*/

// JSON Object
/*
	JSON - JavaScript Object Notation
			  - used for serializing/deserializing different data types into byte
			  - byte - binary language (1 & 0) which is used to represent a character
		Serialization - process of converting data into series of bytes for easier transmission or transfer of information
*/


/*let city = {
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
};
console.log(city);*/

// JSON Arrays
/*let cities = [
{
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
},
{
	"city": "Bacoor",
	"province": "Cavite",
	"country": "Philippines"
},
{
	"city": "New York",
	"province": "New York",
	"country": "U.S.A."
}
];
console.log(cities);*/


// JSON METHODS

	// JSON object contains methods for parsing and converting data into stringified JSON
	// Stringified JSON - JSON Object/JS objects converted into string to be used in other functions of the language esp. javascript-based applications (serialize);
let batches = [
{
	batchName: "Batch X"
},
{
	batchName: "Batch Y"
}
];
console.log("Result from console.log method");
console.log(batches);

console.log("Result from stringify method");
// stringify - used to convert JSON Objects into JSON (string)
console.log(JSON.stringify(batches));

// using stringify for JS Objects
let data = JSON.stringify({
	name: "John",
	age: 31,
	address:{
		city: "Manila",
		country: "Philippines"
	}
});
console.log(data);


/*
Assignment
	create userDatails variable that will contain js object with the following properties
		fname - prompt
		lname - prompt
		age - prompt
		address:{
			city - prompt
			country - prompt
			zipCode - prompt
		}

	log in the console the converted JSON data type
*/
/*let userDetails = JSON.stringify(
{
	fname:prompt('Please input your first name.'),
	lname:prompt('Please input your last name.'),
	age:prompt('Please input your age.'), 
	address: {
		city: prompt('City'),
		country: prompt('Country'),
		zipCode: prompt('Zipcode')
	}
}
);*/
/*	fname = prompt("What is your first name?");
	lname =  prompt("What is your last name?");
	age = prompt("How old are you");

	city = prompt("What city are in?");
	country = prompt("What Country?");
	zipcode = prompt("The zipcode is?");

	let userDetails = JSON.stringify({
	    first_name: fname,
	    last_name: lname,
	    age2: age,
	    Address:{
	        city2: city,
	        country2: country,
	        zipcode2: zipcode
	    }
	}

);*/
// console.log(userDetails);


// CONVERTING OF STRINGIFIED JSON INTO JS OBJECTS
	// PARSE Method - converting json data into js objects
	// information is commonly sent to application in STINGIFIED JSON; then converted into objects; this happens both for sending information to a backend app such as databases and back to frontend app such as the webpages
	// upon receiving the data, JSON text can be converted into a JS/JSON Object with parse method
let batchesJSON = `[
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
]`;
console.log(batchesJSON);

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedData = `{
	"name": "John",
	"age": "31",
	"address":{
		"city": "Manila",
		"country": "Philippines"
	}
}`;
// how do we convert the JSON string above into a JS object and log it in our console?
console.log	(JSON.parse(stringifiedData));
